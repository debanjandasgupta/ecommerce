const expr = require('express');

const productTasks = require('../controller/product_activity_controller')
let router = expr.Router();


router.route("/add")
    .post(productTasks.authToken, productTasks.add_product);
router.route("/delete")
    .post(productTasks.authToken, productTasks.delete_product);
router.route("/update")
    .post(productTasks.authToken, productTasks.update_product);
router.route("/get_all")
    .post(productTasks.get_products);
router.route("/add_cart")
    .post(productTasks.authToken, productTasks.products_add_to_cart);
router.route("/show_cart")
    .post(productTasks.authToken, productTasks.products_show_cart);
router.route("/remove_from_cart")
    .post(productTasks.authToken, productTasks.delete_from_cart);

module.exports = router;