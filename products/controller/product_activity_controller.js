require('dotenv').config()
const jwt = require('jsonwebtoken');
const validator = require('../../validator/index')
const models = require('../../models/index')

exports.authToken = (req, res, next) => {
    const tokenHeader = req.headers['authorization'];
    const token = tokenHeader && tokenHeader.split(" ")[1];
    if (token === null) {
        return res.send({
            status_code: 401,
            message: 'No token provided'
        });
    }

    jwt.verify(token, process.env.PRIVATE_KEY, (err, user) => {
        if (err) {
            console.log(err);
            return res.send({
                status_code: 403,
                message: "Token is no longer valid."
            });
        }
        req.user = user;
        next();
    })
}

exports.add_product = async (req, res) => {
    if (req.user.role === "ADMIN") {
        try {
            const results = await validator.product_add_validator.validateAsync({
                product_name: req.body.name,
                product_description: req.body.desc,
                product_price: req.body.price
            });
            models.product.create({
                name: results.product_name,
                description: results.product_description,
                price: results.product_price
            }).then(() => {
                let s = {
                    status_code: 200,
                    message: `Product added.`
                };
                res.send(s);
            })
        }
        catch (err) {
            res.send({
                status_code: 422,
                message: err.message
            });
        }
    }
    else {
        res.send({
            status_code: 213,
            message: "Authenctication error."
        });
    }
}



exports.delete_product = async (req, res) => {
    if (req.user.role === "ADMIN") {
        try {
            const results = await validator.product_delete_validator.validateAsync({
                product_id: req.body.id
            });
            models.product.destroy({ where: { id: results.product_id } }).then(() => {
                let s = {
                    status_code: 200,
                    message: `Product deleted.`
                };
                res.send(s);
            })
        }
        catch (err) {
            res.send({
                status_code: 422,
                message: err.message
            });
        }
    }
    else {
        res.send({
            status_code: 213,
            message: "Authenctication error."
        });
    }
}

exports.update_product = async (req, res) => {
    if (req.user.role === "ADMIN") {
        try {
            const results = await validator.product_update_validator.validateAsync({
                product_id: req.body.id,
                product_name: req.body.name,
                product_description: req.body.desc,
                product_price: req.body.price
            });
            if (results.product_name != undefined) {
                models.product.update({ name: results.product_name }, { where: { id: results.product_id } });
            }
            if (results.product_description != undefined) {
                models.product.update({ description: results.product_description }, { where: { id: results.product_id } });
            }
            if (results.product_price != undefined) {
                models.product.update({ price: results.product_price }, { where: { id: results.product_id } });
            }
            let s = {
                status_code: 200,
                message: `Product updated.`
            };
            res.send(s);
        }
        catch (err) {
            res.send({
                status_code: 422,
                message: err.message
            });
        }
    }
    else {
        res.send({
            status_code: 213,
            message: "Authenctication error."
        });
    }
}



exports.get_products = async (req, res) => {
    const products_data = await models.product.findAll({ attributes: ['id', 'name', 'description', 'price'] });
    let data = products_data.map(products => {
        return {
            Id: products.id,
            Name: products.name,
            Description: products.description,
            Price: products.price
        };
    });
    res.send(data);
}

exports.products_add_to_cart = async (req, res) => {
    if (req.user.role === "USER") {
        try {
            const results = await validator.view_cart_validator.validateAsync({
                product_id: req.body.product_id,
                user_id: req.user.id
            });
            models.cart.create({
                userId: results.user_id,
                productId: results.product_id
            }).then(() => {
                let s = {
                    status_code: 200,
                    message: `Product added to cart.`
                };
                res.send(s);
            })
        }
        catch (err) {
            res.send({
                status_code: 422,
                message: err.message
            });
        }
    }
    else {
        res.send({
            status_code: 213,
            message: "Authenctication error."
        });
    }
}

exports.products_show_cart = async (req, res) => {
    if (req.user.role === "USER") {
        try {
            const results = await validator.view_cart_validator.validateAsync({
                user_id: req.user.id
            });
            const fdata = await models.cart.findAll({
                include: [models.product],
                where: {
                    userId: results.user_id
                },
                attributes: ['id']
            })
            let sendData = [];
            let totPrice = 0;
            fdata.forEach(element => {
                let s = {
                    Quantity: 1,
                    product_id: element.product.id,
                    Product_Name: element.product.name,
                    Product_Description: element.product.description,
                    Product_Price: element.product.price
                };
                totPrice = totPrice + element.product.price;
                sendData.push(s);
            });
            res.send({
                all_product_data: sendData,
                total_price: totPrice
            });
        }
        catch (err) {
            res.send({
                status_code: 422,
                message: err.message
            });
        }
    }
    else {
        res.send({
            status_code: 213,
            message: "Authenctication error."
        });
    }
}




exports.delete_from_cart = async (req, res) => {
    if (req.user.role === "USER") {
        try {
            const results = await validator.cart_delete_validator.validateAsync({
                cart_reference_id: req.body.id
            });
            models.cart.destroy({ where: { id: results.cart_reference_id } }).then(() => {
                let s = {
                    status_code: 200,
                    message: `Product removed from cart.`
                };
                res.send(s);
            })
        }
        catch (err) {
            res.send({
                status_code: 422,
                message: err.message
            });
        }
    }
    else {
        res.send({
            status_code: 213,
            message: "Authenctication error."
        });
    }
}