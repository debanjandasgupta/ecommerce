const product = require('./products')
const user = require('./users')
const cart = require('./cart')
cart.belongsTo(user);
cart.belongsTo(product);