const Sequelize = require('sequelize');
const User = require('./users');
const sequelize = require('../db/db');
const Products = require('./products');


const Cart = sequelize.define("cart", {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    userId: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    productId: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    createdAt: {
        type: Sequelize.DATE
    },
    updatedAt: {
        type: Sequelize.DATE
    },
    deletedAt: {
        type: Sequelize.DATE
    }
});

Cart.associate = models => {
    User.hasMany(models.Cart);
    Products.hasMany(models.Cart);
}



module.exports = Cart;