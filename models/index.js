module.exports = {
    user: require('./users'),
    product: require('./products'),
    cart: require('./cart'),
    connection: require('./Connection')
}