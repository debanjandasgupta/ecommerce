const Joi = require('joi');

const loginSchema = Joi.object({
    email: Joi.string().email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } }).lowercase().required(),
    password: Joi.string().min(4).max(20).required()
});


module.exports = loginSchema;
