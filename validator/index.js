module.exports = {
    admin_login_validator: require('./admin_login_validator'),
    admin_registration_validator: require('./admin_registration_validator'),
    product_add_to_cart_validator: require('./product_add_to_cart_validation'),
    cart_delete_validator: require('./cart_delete_validator'),
    product_delete_validator: require('./product_delete_validator'),
    product_update_validator: require('./product_update_validator'),
    product_add_validator: require('./product_validator'),
    user_login_validator: require('./user_login_validator'),
    user_registration_validator: require('./user_registration_validator'),
    view_cart_validator: require('./view_cart_data_validation')
}