const Joi = require('joi');

const productSchema = Joi.object({
    cart_reference_id: Joi.number().integer().required()
});


module.exports = productSchema;
