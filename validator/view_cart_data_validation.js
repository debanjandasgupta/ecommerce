const Joi = require('joi');

const productSchema = Joi.object({
    user_id: Joi.number().integer().required()
});


module.exports = productSchema;
