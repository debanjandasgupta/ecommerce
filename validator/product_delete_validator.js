const Joi = require('joi');

const productSchema = Joi.object({
    product_id: Joi.number().integer().required()
});


module.exports = productSchema;
