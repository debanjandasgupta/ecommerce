const Joi = require('joi');

const productSchema = Joi.object({
    product_id: Joi.number().integer().required(),
    product_name: Joi.string(),
    product_description: Joi.string(),
    product_price: Joi.number().integer()
});


module.exports = productSchema;
