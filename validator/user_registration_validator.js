const Joi = require('joi');

const registrationSchema = Joi.object({
    name: Joi.string().required(),
    email: Joi.string().email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } }).lowercase().required(),
    password: Joi.string().min(4).max(20).required(),
    role: Joi.string().max(5).default("USER")
});


module.exports = registrationSchema;
