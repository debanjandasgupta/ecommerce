const sequelize = require('../../db/db');
const models = require('../../models/index')
const validator = require('../../validator/index')


exports.registrationMaker = async (req, res) => {
    try {
        const results = await validator.admin_registration_validator.validateAsync({
            name: req.body.name,
            email: req.body.email,
            password: req.body.password,
            role: req.body.role
        });
        if (results.role === "USER") {
            return res.send({
                status_code: 205,
                message: "Go to user registration."
            });
        }
        else {
            let count = await models.user.count({
                where: {
                    email: results.email
                }
            });
            if (count > 0) {
                let s = {
                    status_code: 204,
                    message: `Email already exists.`
                };
                res.send(s);
            }
            else {
                models.user.create({
                    name: results.name,
                    email: results.email,
                    password: results.password,
                    role: results.role
                }).then(() => {
                    let s = {
                        status_code: 200,
                        message: `welcome ${results.name}`
                    };
                    res.send(s);
                })
            }
        }
    }
    catch (err) {
        res.send({
            status_code: 422,
            message: err.message
        });
    }
};