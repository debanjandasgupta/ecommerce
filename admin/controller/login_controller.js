require('dotenv').config()
const sequelize = require('../../db/db');
const models = require('../../models/index')
const validator = require('../../validator/index');
const jwt = require('jsonwebtoken');

exports.loginMaker = async (req, res) => {
    try {
        const results = await validator.admin_login_validator.validateAsync({
            email: req.body.email,
            password: req.body.password
        });
        let count = await models.user.count({
            where: {
                email: results.email,
                password: results.password,
                role: "ADMIN"
            }
        });
        if (count > 0) {
            models.user.findOne({
                where: {
                    email: results.email,
                    password: results.password,
                    role: "ADMIN"
                },
                attributes: ['id', 'name', 'role']
            }).then((users) => {
                const user = {
                    status_code: 200,
                    id: users.id,
                    fullname: users.name,
                    role: users.role
                };
                let accessToken = jwt.sign(user, process.env.PRIVATE_KEY);
                res.json(accessToken);
            });
        }
        else {
            res.send({
                status_code: 205,
                message: "Go to user login."
            });
        }
    }
    catch (err) {
        res.send({
            status_code: 422,
            message: err.message
        });
    }
};