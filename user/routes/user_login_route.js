const expr = require('express');
const loginChecker = require('../controller/login_controller')
let router = expr.Router();


router.route("")
    .post(loginChecker.loginMaker);

module.exports = router;