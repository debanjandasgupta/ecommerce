require('dotenv').config()
const jwt = require('jsonwebtoken');
const sequelize = require('../../db/db');
const models = require('../../models/index')
const validator = require('../../validator/index')


exports.loginMaker = async (req, res) => {
    try {
        const results = await validator.user_login_validator.validateAsync({
            email: req.body.email,
            password: req.body.password
        });
        let count = await models.user.count({
            where: {
                email: results.email,
                password: results.password,
                role: "USER"
            }
        });
        if (count > 0) {
            models.user.findOne({
                where: {
                    email: results.email,
                    password: results.password,
                    role: "USER"
                },
                attributes: ['id', 'name', 'role']
            }).then((users) => {
                const user = {
                    status_code: 200,
                    id: users.id,
                    fullname: users.name,
                    role: users.role
                };
                let accessToken = jwt.sign(user, process.env.PRIVATE_KEY);
                res.json(accessToken);
            });
        }
        else {
            res.send({
                status_code: 205,
                message: "Go to admin login."
            });
        }
    }
    catch (err) {
        res.send({
            status_code: 422,
            message: err.message
        });
    }
};
