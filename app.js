const expr = require('express');
const app = expr();
const userLogin = require('./user/routes/user_login_route')
const userReg = require('./user/routes/user_registration_route')
const adminLogin = require('./admin/routes/admin_login_route')
const adminReg = require('./admin/routes/admin_registration_route')
const product = require('./products/routes/product_routes')


let jsonParser = require('body-parser').json();

//User Login
app.use("/user/login", jsonParser, userLogin);
//User Registration
app.use("/user/register", jsonParser, userReg);
//Admin Login
app.use("/admin/login", jsonParser, adminLogin);
//Admin Register
app.use("/admin/register", jsonParser, adminReg);
//Product Functions
app.use("/product", jsonParser, product);
//Cart Functions
app.use("/cart", jsonParser, product)


const port = process.env.port || 3000;
app.listen(port, () => console.log(`Listening to ${port}...`))